﻿using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Program
    {
        public static HashSet<string> BanList { get; } = new HashSet<string>();

        private static Dictionary<string, TcpClient> clientsList = new Dictionary<string, TcpClient>();
        private static readonly object locker = new object();
        private static bool running = true;

        static void ThreadTask()
        {
            var localhost = IPAddress.Parse("127.0.0.1");
            TcpListener serverSocket = new TcpListener(localhost, 8888);
            TcpClient clientSocket = default(TcpClient);
            int counter = 0;

            serverSocket.Start();

            Console.WriteLine("Chat Server Started ....");

            counter = 0;

            while (running)
            {
                counter += 1;
                clientSocket = serverSocket.AcceptTcpClient();
                byte[] bytesFrom = new byte[70000];
                string dataFromClient = null;

                NetworkStream networkStream = clientSocket.GetStream();
                networkStream.Read(bytesFrom, 0, clientSocket.ReceiveBufferSize);
                dataFromClient = Encoding.ASCII.GetString(bytesFrom);
                dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                lock (locker)
                {
                    clientsList.Add(dataFromClient, clientSocket);
                }
                Broadcast(dataFromClient + " Joined ", dataFromClient, false);
                Console.WriteLine(dataFromClient + " Joined chat room ");

                HandleClient client = new HandleClient();
                client.startClient(clientSocket, dataFromClient, clientsList);
            }
            clientSocket.Close();
            serverSocket.Stop();
        }

        static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem((arg) => ThreadTask());
            while (running)
            {
                Console.Write(">>> ");
                var input = Console.ReadLine().Trim().Split();
                string command = input[0];
                string arg = "";
                if (input.Length == 2)
                {
                    arg = input[1];
                }
                switch (command)
                {
                    case "exit":
                        lock (locker)
                        {
                            running = false;
                        }
                        break;
                    case "ban":
                        BanList.Add(arg);
                        break;
                    case "unban":
                        BanList.Remove(arg);
                        break;
                    default:
                        lock (locker)
                        {
                            running = false;
                        }
                        break;
                }
            }
        }

        public static void Broadcast(string msg, string uName, bool flag)
        {
            foreach (KeyValuePair<string, TcpClient> item in clientsList)
            {
                var  broadcastSocket = item.Value;
                NetworkStream broadcastStream = broadcastSocket.GetStream();
                byte[] broadcastBytes = null;

                if (flag == true)
                {
                    broadcastBytes = Encoding.ASCII.GetBytes(uName + " says : " + msg);
                }
                else
                {
                    broadcastBytes = Encoding.ASCII.GetBytes(msg);
                }
                broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length);
                broadcastStream.Flush();
            }

        }  //end broadcast function

    }//end Main class

    public class HandleClient
    {
        TcpClient clientSocket;
        string clNo;
        Dictionary<string, TcpClient> clientsList;

        public void startClient(TcpClient inClientSocket, string clineNo, Dictionary<string, TcpClient> cList)
        {
            this.clientSocket = inClientSocket;
            this.clNo = clineNo;
            this.clientsList = cList;
            Thread ctThread = new Thread(doChat);

            ctThread.Start();
        }

        private void doChat()
        {
            int requestCount = 0;
            byte[] bytesFrom = new byte[70000];
            string dataFromClient = null;
            string rCount = null;
            requestCount = 0;

            while (true)
            {
                try
                {
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    networkStream.Read(bytesFrom, 0, clientSocket.ReceiveBufferSize);
                    dataFromClient = Encoding.ASCII.GetString(bytesFrom);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                    if (Program.BanList.Contains(clNo))
                    {
                        continue;
                    }
                    Console.WriteLine("From client - " + clNo + " : " + dataFromClient);
                    rCount = Convert.ToString(requestCount);

                    Program.Broadcast(dataFromClient, clNo, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }//end while

        }//end doChat

    } //end class handleClinet

}//end namespace

