﻿using System;
using System.Windows.Forms;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    public partial class Form1 : Form
    {
        TcpClient clientSocket = new TcpClient();
        NetworkStream serverStream = default(NetworkStream);
        string readData = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            readData = "Connecting to Chat Server ...";
            msg();

            var ipAddress = textBox_IPAddress.Text;

            if (ipAddress == "localhost")
            {
                ipAddress = "127.0.0.1";
            }

            clientSocket.Connect(ipAddress, 8888);
            serverStream = clientSocket.GetStream();

            byte[] outStream = Encoding.ASCII.GetBytes(textBox_Name.Text + "$");
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();

            Thread ctThread = new Thread(getMessage);
            ctThread.Start();
        }

        private void getMessage()
        {
            while (true)
            {
                serverStream = clientSocket.GetStream();
                int buffSize = 0;
                byte[] inStream = new byte[70000];
                buffSize = clientSocket.ReceiveBufferSize;
                serverStream.Read(inStream, 0, buffSize);
                string returndata = Encoding.ASCII.GetString(inStream);
                readData = "" + returndata;
                msg();
            }
        }

        private void msg()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(msg));
            else
                textBox_Chat.Text = textBox_Chat.Text + Environment.NewLine + " >> " + readData;
        }

        private void btn_Send_Click(object sender, EventArgs e)
        {
            byte[] outStream = Encoding.ASCII.GetBytes(textBox_Message.Text + "$");
            textBox_Message.Clear();
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }
    }
}
