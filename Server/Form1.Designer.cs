﻿namespace Server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_TurnOff = new System.Windows.Forms.Button();
            this.btn_Ban = new System.Windows.Forms.Button();
            this.textBox_GeneralInfo = new System.Windows.Forms.TextBox();
            this.textBox_ClientName = new System.Windows.Forms.TextBox();
            this.btn_TurnOn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_TurnOff
            // 
            this.btn_TurnOff.Location = new System.Drawing.Point(294, 68);
            this.btn_TurnOff.Name = "btn_TurnOff";
            this.btn_TurnOff.Size = new System.Drawing.Size(100, 23);
            this.btn_TurnOff.TabIndex = 0;
            this.btn_TurnOff.Text = "Turn off Server";
            this.btn_TurnOff.UseVisualStyleBackColor = true;
            this.btn_TurnOff.Click += new System.EventHandler(this.btn_TurnOff_Click);
            // 
            // btn_Ban
            // 
            this.btn_Ban.Location = new System.Drawing.Point(410, 10);
            this.btn_Ban.Name = "btn_Ban";
            this.btn_Ban.Size = new System.Drawing.Size(100, 23);
            this.btn_Ban.TabIndex = 1;
            this.btn_Ban.Text = "Ban client";
            this.btn_Ban.UseVisualStyleBackColor = true;
            this.btn_Ban.Click += new System.EventHandler(this.btn_Ban_Click);
            // 
            // textBox_GeneralInfo
            // 
            this.textBox_GeneralInfo.Location = new System.Drawing.Point(12, 12);
            this.textBox_GeneralInfo.Multiline = true;
            this.textBox_GeneralInfo.Name = "textBox_GeneralInfo";
            this.textBox_GeneralInfo.Size = new System.Drawing.Size(266, 254);
            this.textBox_GeneralInfo.TabIndex = 2;
            // 
            // textBox_ClientName
            // 
            this.textBox_ClientName.Location = new System.Drawing.Point(294, 12);
            this.textBox_ClientName.Name = "textBox_ClientName";
            this.textBox_ClientName.Size = new System.Drawing.Size(100, 20);
            this.textBox_ClientName.TabIndex = 3;
            // 
            // btn_TurnOn
            // 
            this.btn_TurnOn.Location = new System.Drawing.Point(294, 39);
            this.btn_TurnOn.Name = "btn_TurnOn";
            this.btn_TurnOn.Size = new System.Drawing.Size(100, 23);
            this.btn_TurnOn.TabIndex = 4;
            this.btn_TurnOn.Text = "Turn On Server";
            this.btn_TurnOn.UseVisualStyleBackColor = true;
            this.btn_TurnOn.Click += new System.EventHandler(this.btn_TurnOn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 277);
            this.Controls.Add(this.btn_TurnOn);
            this.Controls.Add(this.textBox_ClientName);
            this.Controls.Add(this.textBox_GeneralInfo);
            this.Controls.Add(this.btn_Ban);
            this.Controls.Add(this.btn_TurnOff);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_TurnOff;
        private System.Windows.Forms.Button btn_Ban;
        private System.Windows.Forms.TextBox textBox_GeneralInfo;
        private System.Windows.Forms.TextBox textBox_ClientName;
        private System.Windows.Forms.Button btn_TurnOn;
    }
}

